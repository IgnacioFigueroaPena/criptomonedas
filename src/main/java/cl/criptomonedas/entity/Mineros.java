/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.criptomonedas.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Personal
 */
@Entity
@Table(name = "mineros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mineros.findAll", query = "SELECT m FROM Mineros m"),
    @NamedQuery(name = "Mineros.findByPlacaMadre", query = "SELECT m FROM Mineros m WHERE m.placaMadre = :placaMadre"),
    @NamedQuery(name = "Mineros.findByProcesador", query = "SELECT m FROM Mineros m WHERE m.procesador = :procesador"),
    @NamedQuery(name = "Mineros.findByFuenteDePoder", query = "SELECT m FROM Mineros m WHERE m.fuenteDePoder = :fuenteDePoder"),
    @NamedQuery(name = "Mineros.findByRam", query = "SELECT m FROM Mineros m WHERE m.ram = :ram"),
    @NamedQuery(name = "Mineros.findByGabineteriel", query = "SELECT m FROM Mineros m WHERE m.gabineteriel = :gabineteriel")})
public class Mineros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "placa madre")
    private String placaMadre;
    @Size(max = 2147483647)
    @Column(name = "procesador ")
    private String procesador;
    @Size(max = 2147483647)
    @Column(name = "fuente de poder ")
    private String fuenteDePoder;
    @Size(max = 2147483647)
    @Column(name = "ram")
    private String ram;
    @Size(max = 2147483647)
    @Column(name = "gabineteriel ")
    private String gabineteriel;

    public Mineros() {
    }

    public Mineros(String placaMadre) {
        this.placaMadre = placaMadre;
    }

    public String getPlacaMadre() {
        return placaMadre;
    }

    public void setPlacaMadre(String placaMadre) {
        this.placaMadre = placaMadre;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getFuenteDePoder() {
        return fuenteDePoder;
    }

    public void setFuenteDePoder(String fuenteDePoder) {
        this.fuenteDePoder = fuenteDePoder;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getGabineteriel() {
        return gabineteriel;
    }

    public void setGabineteriel(String gabineteriel) {
        this.gabineteriel = gabineteriel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placaMadre != null ? placaMadre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mineros)) {
            return false;
        }
        Mineros other = (Mineros) object;
        if ((this.placaMadre == null && other.placaMadre != null) || (this.placaMadre != null && !this.placaMadre.equals(other.placaMadre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.criptomonedas.entity.Mineros[ placaMadre=" + placaMadre + " ]";
    }
    
}
