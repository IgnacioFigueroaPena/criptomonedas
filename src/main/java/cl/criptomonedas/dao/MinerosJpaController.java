/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.criptomonedas.dao;

import cl.criptomonedas.dao.exceptions.NonexistentEntityException;
import cl.criptomonedas.dao.exceptions.PreexistingEntityException;
import cl.criptomonedas.entity.Mineros;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Personal
 */
public class MinerosJpaController implements Serializable {

    public MinerosJpaController() {
       }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_CRIPTOMONEDAS");


    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Mineros mineros) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(mineros);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMineros(mineros.getPlacaMadre()) != null) {
                throw new PreexistingEntityException("Mineros " + mineros + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Mineros mineros) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            mineros = em.merge(mineros);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = mineros.getPlacaMadre();
                if (findMineros(id) == null) {
                    throw new NonexistentEntityException("The mineros with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mineros mineros;
            try {
                mineros = em.getReference(Mineros.class, id);
                mineros.getPlacaMadre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mineros with id " + id + " no longer exists.", enfe);
            }
            em.remove(mineros);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Mineros> findMinerosEntities() {
        return findMinerosEntities(true, -1, -1);
    }

    public List<Mineros> findMinerosEntities(int maxResults, int firstResult) {
        return findMinerosEntities(false, maxResults, firstResult);
    }

    private List<Mineros> findMinerosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Mineros.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Mineros findMineros(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Mineros.class, id);
        } finally {
            em.close();
        }
    }

    public int getMinerosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Mineros> rt = cq.from(Mineros.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
