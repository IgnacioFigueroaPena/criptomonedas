
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Criptomonedas</title>
  <style>
*{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
body{margin:0px; padding:0px; font-family: 'Open Sans', sans-serif;}
section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
h1{margin:0px auto; display:table; font-size:26px; padding:40px 0px; color:#002e5b; text-align:center;}
h1 span{font-weight:500;}

header{width:100%; display:table; background-color:#FF8C00; margin-bottom:50px;}
#logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
nav{width:auto; float:right;}
nav ul{display:table; float:right;}
nav ul li{float:left;}
nav ul li:last-child{padding-right:0px;}
nav ul li a{color:#FFFFFF; font-size:18px; padding: 25px 20px; display:inline-block; transition: all 0.5s ease 0s;}
nav ul li a:hover{background-color:#FFFFFF; color:#fde428; transition: all 0.5s ease 0s;}
nav ul li a:hover i{color:#FF8C00; transition: all 0.5s ease 0s;}
nav ul li a i{padding-right:10px; color:#002e5b; transition: all 0.5s ease 0s;}

.toggle-menu ul{display:table; width:25px;}
.toggle-menu ul li{width:100%; height:3px; background-color:#002e5b; margin-bottom:4px;}
.toggle-menu ul li:last-child{margin-bottom:0px;}

input[type=checkbox], label{display:none;}

.content{display:table; margin-bottom:60px; width:900px;}
.content h2{font-size:18px; font-weight:500; color:#002e5b; border-bottom:1px solid #fde428; display:table; padding-bottom:10px; margin-bottom:10px;}
.content p{font-size:14px; line-height:22px; color:#7c7c7c; text-align:justify;}

footer{display:table; padding-bottom:30px; width:100%;}
.social{margin:0px auto; display:table; display:table;}
.social li{float:left; padding:0px 10px;}
.social li a{color:#002e5b; transition: all 0.5s ease 0s;}
.social li a:hover{color:#fde428; transition: all 0.5s ease 0s;}

@media only screen and (max-width: 1440px) {
section{max-width:95%;}
}

@media only screen and (max-width: 980px) {
header{padding:20px 0px;}
#logo{padding:0px;}
input[type=checkbox] {position: absolute; top: -9999px; left: -9999px; background:none;}
input[type=checkbox]:fous{background:none;}
label {float:right; padding:8px 0px; display:inline-block; cursor:pointer; }
input[type=checkbox]:checked ~ nav {display:block;}

nav{display:none; position:absolute; right:0px; top:53px; background-color:#002e5b; padding:0px; z-index:99;}
nav ul{width:auto;}
nav ul li{float:none; padding:0px; width:100%; display:table;}
nav ul li a{color:#FFF; font-size:15px; padding:10px 20px; display:block; border-bottom: 1px solid rgba(225,225,225,0.1);}
nav ul li a i{color:#fde428; padding-right:13px;}
}

@media only screen and (max-width: 980px) {
.content{width:90%;}
}

@media only screen and (max-width: 568px) {
h1{padding:25px 0px;}
h1 span{display:block;}
}

@media only screen and (max-width: 480px) {
section {max-width: 90%;}
}

@media only screen and (max-width: 360px) {
h1{font-size:20px;}
label{padding:5px 0px;}
#logo{font-size: 20px;}
nav{top:47px;}
}

@media only screen and (max-width: 320px) {
h1 {padding: 20px 0px;}
}

</style>
</head>
    <body>
        <div align="center"
        <h1><span>Criptomonedas</span> </h1>
<h3>Registrate y comienza hacer trading</h3>
        </div>
        
<header>
<section>
<a href="yur" id="logo" target="_blank"></a>

<label for="toggle-1" class="toggle-menu"><ul><li></li> <li></li> <li></li></ul></label>
<input type="checkbox" id="toggle-1">

<nav>
<ul>
<li><a href="RegistrarController"><i class="icon-home"></i>Registrarse</a></li>
<li><a href="https://www.bbva.com/es/que-es-trading-que-hace-falta-para-operar/"><i class="icon-user"></i>¿Que es trading?</a></li>
<li><a href="#portfolio"><i class="icon-thumbs-up-alt"></i> Mercados </a></li>
<li><a href="MinarController"><i class="icon-gear"></i>Minar</a></li>
<li><a href="#gallery"><i class="icon-picture"></i>Finanzas</a></li>
<li><a href="#contact"><i class="icon-phone"></i>Comprar Criptomodeas</a></li>
</ul>
</nav>
</header>

        <section id="about" class="content">
<h2>Criptomoneda</h2>
<p>La criptomoneda, también llamada moneda virtual o criptodivisa, es dinero digital. 
Eso significa que no hay monedas ni billetes físicos — todo es en línea. 
Usted puede transferirle una criptomoneda a alguien en internet sin un intermediario, como un banco. 
Las criptomonedas más conocidas son Bitcoin y Ether, pero se continúan creando nuevas cripto-monedas.

La gente podría usar criptomonedas para hacer pagos rápidos y para evitar los cargos de transacción. Algunas personas podrían adquirir criptomonedas como una inversión, con la esperanza de que aumente su valor. Las cripto-monedas se pueden comprar con una tarjeta de crédito o, en algunos casos, a través de un proceso llamado “minería”. Las criptomonedas se almacenan en un monedero o cartera digital, ya sea en línea, en su computadora o en otro soporte físico.

Antes de comprar una criptomoneda, tiene que saber que no tiene las mismas protecciones que cuando usa dólares estadounidenses. También tiene que saber que los estafadores le están pidiendo a la gente que le paguen con una criptomoneda porque saben que, por lo general, esos pagos son irreversibles.


</p>

<h2>Criptomonedas versus dólares estadounidenses</h2>

<p> El hecho de que las cripto-monedas sean digitales no es la única diferencia importante entre las criptomonedas y las monedas tradicionales como los dólares estadounidenses.
Las criptomonedas, no están respaldadas por un gobierno.

Las criptomonedas no están aseguradas por el gobierno como sí lo están los depósitos bancarios en EE. UU. Eso significa que las cripto-monedas almacenadas en línea no tienen las mismas protecciones que tiene el dinero depositado en una cuenta bancaria. Si usted almacena una cripto-moneda en una cartera o monedero digital provisto por una compañía, y la compañía cesa sus operaciones o sufre un ataque informático, es posible que el gobierno no pueda actuar y ayudarlo a recuperar el dinero como podría hacerlo con el dinero que se guarda en los bancos o cooperativas de crédito.
El valor de una cripto-moneda cambia constantemente.

El valor de una criptomoneda puede cambiar cada hora. Una inversión que hoy puede tener un valor de miles de dólares mañana podría valer solo cientos de dólares. Si el valor baja, no hay garantía de que vuelva a subir.
</p><!-- comment -->

<h2>¿Está por invertir en criptomonedas?</h2>

<p>
    
    Al igual que con cualquier otra inversión, antes de invertir en una criptomoneda, sepa cuáles son los riesgos y aprenda cómo detectar una estafa. A continuación una lista de algunas de las cosas con las que hay que tener cuidado al momento de considerar sus opciones.
Nadie le puede garantizar que ganará dinero.

Todo aquel que le prometa un rendimiento o dividendo garantizado probablemente sea un estafador. El solo hecho de que una inversión sea muy conocida o esté endosada por una celebridad no significa que sea algo bueno o seguro. Eso se aplica tanto a las criptomonedas como a las inversiones más tradicionales. No invierta dinero que no puede permitirse perder.
No todas las criptomonedas — o las compañías que promocionan cripto-monedas — son iguales.

Fíjese en las declaraciones que están haciendo las compañías que están promocionando criptomonedas. Busque en internet ingresando el nombre de la compañía y de la criptomoneda y agregue palabras como “review”, “scam” o “complaint”, si hace la búsqueda en español, agregue palabras como "comentario", "estafa" o "queja".
    
    
</p>


        </section><!-- comment -->
        


        
        
    </body>
 </html>
