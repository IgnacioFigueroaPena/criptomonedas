
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Criptomonedas</title>
  <style>
*{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
body{margin:0px; padding:0px; font-family: 'Open Sans', sans-serif;}
section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
h1{margin:0px auto; display:table; font-size:26px; padding:40px 0px; color:#002e5b; text-align:center;}
h1 span{font-weight:500;}

header{width:100%; display:table; background-color:#FF8C00; margin-bottom:50px;}
#logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
nav{width:auto; float:right;}
nav ul{display:table; float:right;}
nav ul li{float:left;}
nav ul li:last-child{padding-right:0px;}
nav ul li a{color:#FFFFFF; font-size:18px; padding: 25px 20px; display:inline-block; transition: all 0.5s ease 0s;}
nav ul li a:hover{background-color:#FFFFFF; color:#fde428; transition: all 0.5s ease 0s;}
nav ul li a:hover i{color:#FF8C00; transition: all 0.5s ease 0s;}
nav ul li a i{padding-right:10px; color:#002e5b; transition: all 0.5s ease 0s;}


.toggle-menu ul{display:table; width:25px;}
.toggle-menu ul li{width:100%; height:3px; background-color:#002e5b; margin-bottom:4px;}
.toggle-menu ul li:last-child{margin-bottom:0px;}

input[type=checkbox], label{display:none;}

.content{display:table; margin-bottom:60px; width:900px;}
.content h2{font-size:18px; font-weight:500; color:#002e5b; border-bottom:1px solid #fde428; display:table; padding-bottom:10px; margin-bottom:10px;}
.content p{font-size:14px; line-height:22px; color:#7c7c7c; text-align:justify;}

footer{display:table; padding-bottom:30px; width:100%;}
.social{margin:0px auto; display:table; display:table;}
.social li{float:left; padding:0px 10px;}
.social li a{color:#002e5b; transition: all 0.5s ease 0s;}
.social li a:hover{color:#fde428; transition: all 0.5s ease 0s;}

@media only screen and (max-width: 1440px) {
section{max-width:95%;}
}

@media only screen and (max-width: 980px) {
header{padding:20px 0px;}
#logo{padding:0px;}
input[type=checkbox] {position: absolute; top: -9999px; left: -9999px; background:none;}
input[type=checkbox]:fous{background:none;}
label {float:right; padding:8px 0px; display:inline-block; cursor:pointer; }
input[type=checkbox]:checked ~ nav {display:block;}

nav{display:none; position:absolute; right:0px; top:53px; background-color:#002e5b; padding:0px; z-index:99;}
nav ul{width:auto;}
nav ul li{float:none; padding:0px; width:100%; display:table;}
nav ul li a{color:#FFF; font-size:15px; padding:10px 20px; display:block; border-bottom: 1px solid rgba(225,225,225,0.1);}
nav ul li a i{color:#fde428; padding-right:13px;}
}

@media only screen and (max-width: 980px) {
.content{width:90%;}
}

@media only screen and (max-width: 568px) {
h1{padding:25px 0px;}
h1 span{display:block;}
}

@media only screen and (max-width: 480px) {
section {max-width: 90%;}
}

@media only screen and (max-width: 360px) {
h1{font-size:20px;}
label{padding:5px 0px;}
#logo{font-size: 20px;}
nav{top:47px;}
}

@media only screen and (max-width: 320px) {
h1 {padding: 20px 0px;}
}

</style>
</head>
      <body>
        <div align="center"
        <h1><span>Criptomonedas</span> </h1>
<h1>Registrate y comienza hacer trading</h1>
        </div>
<header>
<section>
<a href="yur" id="logo" target="_blank"></a>

<label for="toggle-1" class="toggle-menu"><ul><li></li> <li></li> <li></li></ul></label>
<input type="checkbox" id="toggle-1">

<nav>
<ul>
<li><a href="RegistrarController"><i class="icon-home"></i>Registrarse</a></li>
<li><a href="https://www.bbva.com/es/que-es-trading-que-hace-falta-para-operar/"><i class="icon-user"></i>¿Que es trading?</a></li>
<li><a href="#portfolio"><i class="icon-thumbs-up-alt"></i> Mercados </a></li>
<li><a href="MinarController"><i class="icon-gear"></i>Minar</a></li>
<li><a href="#gallery"><i class="icon-picture"></i>Finanzas</a></li>
<li><a href="#contact"><i class="icon-phone"></i>Comprar Criptomodeas</a></li>
</ul>
</nav>
</header>

</section>

<section id="about" class="content">
<h2>Hardware</h2>
<p>
    
   <form  name="form" action="MinarController" method="POST">
                    
                    <div class="form-group">
                        Placa Madre
                        <input  step="any" name="placamadre" value=""  class="form-control" required id="placamadre" aria-describedby="nombreHelp">
                     </div>       
                    <br>
                       <div class="form-group">
                       Procesador
                        <input  step="any" name="procesador" value=""  class="form-control" required id="procesador" aria-describedby="nombreHelp">
                     </div> 
                    <br>
                        <div class="form-group">
                       Fuente De Poder
                        <input  name="fuentedepoder" value="" class="form-control" required id="fuentedepoder" aria-describedby="usernameHelp">
                           </div>
                        <br>
                            <div class="form-group">
                                Ram
                                <input  name="ram" value="" class="form-control" required id="ram" aria-describedby="usernameHelp">
                                
                            </div>
                            <br>
                            <div class="form-group">
                                GabineteRiel
                                <input  name="gabineteriel" value="" class="form-control" required id="gabineteriel" aria-describedby="usernameHelp">
                                
                            </div>
                    <button type="submit" name="accion" value="ingresar" class="btn btn-success">Ingresar</button>
          
                </form>
</p>
</section>


    
        
    </body>
 </html>
