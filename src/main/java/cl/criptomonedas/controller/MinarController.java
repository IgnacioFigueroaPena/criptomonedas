/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.criptomonedas.controller;

import cl.criptomonedas.dao.MinerosJpaController;
import cl.criptomonedas.entity.Mineros;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Personal
 */
@WebServlet(name = "MinarController", urlPatterns = {"/MinarController"})
public class MinarController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MinarController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MinarController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       request.getRequestDispatcher("minar.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            /**   processRequest(request, response);
             *
             */
            String placamadre = request.getParameter("placamadre");
            String procesador = request.getParameter("procesador");
            String fuentedepoder = request.getParameter("fuentedepoder");
            String ram = request.getParameter("ram");
            String gabineteriel = request.getParameter("gabineteriel");
            
                                   
            Mineros minero=new Mineros();
            minero.setPlacaMadre(placamadre);
            minero.setProcesador(procesador);
            minero.setGabineteriel(gabineteriel);
            minero.setFuenteDePoder(fuentedepoder);
            minero.setRam(ram);
            MinerosJpaController dao=new MinerosJpaController();
            dao.create(minero);
            
        } catch (Exception ex) {
            
            Logger.getLogger(MinarController.class.getName()).log(Level.SEVERE, null, ex);
        }
       request.getRequestDispatcher("index.jsp").forward(request, response);
     
     
    /**   request.getRequestDispatcher("minar.jsp").forward(request, response);
      * 
      */
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
